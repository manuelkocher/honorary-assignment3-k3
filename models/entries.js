const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const entrySchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    category: {
        type: String,
        required: true,
        default: ''
    },
    dateStart: {
        type: String,
        default: ''
    },
    dateFinish: {
        type: String,
        default: ''
    },
    score:  {
        type: Number,
        min: 1,
        max: 10
    }
}, {
    timestamps: true,
    usePushEach: true
});

var Entries = mongoose.model('Entry', entrySchema);

module.exports = Entries;
